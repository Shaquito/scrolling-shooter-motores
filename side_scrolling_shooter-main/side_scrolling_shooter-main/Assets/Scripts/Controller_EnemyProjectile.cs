﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_EnemyProjectile : Projectile
{
    private GameObject player;

    private Vector3 direction;

    private Rigidbody rb;

    public float enemyProjectileSpeed;

    void Start()
    {
        if (Controller_Player._Player != null)//Si en la variable _player de controller_player hay algo. Player va a ser igual a ese gameObject
        {
            player = Controller_Player._Player.gameObject;
            direction = -(this.transform.localPosition - player.transform.localPosition).normalized; //direction es igual a la posicion local del objeto menos la posicion local del player invertido
        }
        rb = GetComponent<Rigidbody>();
    }

    
    public override void Update()//Mueve el proyectil hacia la direction con la velocidad colocada en enemyprojectilespeed
    {
        rb.AddForce(direction*enemyProjectileSpeed);
        base.Update();
    }
}
