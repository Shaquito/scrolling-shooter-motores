using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover_Izquierda : MonoBehaviour
{

    public float velocidad;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.position += new Vector3 (-velocidad,0,0);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
            Controller_Hud.points+= 10;
        }
        if (collision.gameObject.CompareTag("Laser"))
        {
            Destroy(this.gameObject);
            Controller_Hud.points += 10;
        }
    }


}
