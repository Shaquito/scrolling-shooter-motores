﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    public Vector3 posicionInicial;
    

    void Start()
    {
        posicionInicial = Controller_Player._Player.transform.position;
    }

    void Update()
    {
        GetInput();
    }

    private void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Controller_Player._Player.devolverStats();
            Controller_Player._Player.transform.position = posicionInicial;

            Controller_Player._Player.gameObject.SetActive(true);

            Controller_Hud.gameOver = false;

            Time.timeScale = 1;
            SceneManager.LoadScene(0);
        }
    }
}
